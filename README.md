# Vim
### Method 1 :   
 * Install `vim-gtk` package  

        sudo apt install vim-gtk   

 * Set Vim as a default editor  

        sudo update-alternatives --install /usr/bin/editor editor /usr/bin/vim.gtk 1   
        sudo update-alternatives --set editor /usr/bin/vim.gtk   
        sudo update-alternatives --install /usr/bin/vi vi /usr/bin/vim.gtk 1  
        sudo update-alternatives --set vi /usr/bin/vim.gtk   
        sudo update-alternatives --install /usr/bin/vi vim /usr/bin/vim.gtk 1  
        sudo update-alternatives --set vim /usr/bin/vim.gtk    

### Method 2 : 
Otherwise you need to compile Vim from source code(assuming you're using Debian based system)  

#### Prepare your system
 * Install   
 
        sudo apt install libncurses5-dev \
        libgtk2.0-dev libatk1.0-dev \
        libcairo2-dev python-dev \
        python3-dev git

 * Remove Vim if you already have 

        sudo apt remove vim vim-runtime gvim  

#### configure and make  
    cd /usr && sudo git clone https://github.com/vim/vim.git && cd vim  

    sudo ./configure --with-features=huge \
    --enable-multibyte \
    --enable-pythoninterp=yes \
    --with-python-config-dir=/usr/lib/python2.7/config-x86_64-linux-gnu/ \  # pay attention here check directory correct
    --enable-python3interp=yes \
    --with-python3-config-dir=/usr/lib/python3.5/config-3.5m-x86_64-linux-gnu/ \  # pay attention here check directory correct
    --enable-gui=gtk2 \
    --enable-cscope \ 
    --prefix=/usr/local/

    sudo make VIMRUNTIMEDIR=/usr/local/share/vim/vim81 


#### Preparing deb package using [checkinstall](https://wiki.debian.org/CheckInstall)
  * And install that package   

        cd /usr/vim && sudo checkinstall

  * Or, if want to just create a package use `--install=no` option with checkinstall

#### Set Vim as a default editor  
    sudo update-alternatives --install /usr/bin/editor editor /usr/local/bin/vim 1
    sudo update-alternatives --set editor /usr/local/bin/vim
    sudo update-alternatives --install /usr/bin/vi vi /usr/local/bin/vim 1
    sudo update-alternatives --set vi /usr/local/bin/vim   

#### Verify that you're running the new Vim binary by looking

    vim --version | grep python

### Method 3 : 
  Download [deb package](https://gitlab.com/competitive-programming/vim/raw/master/vim_8.1.0-4+deb9u1-1_amd64.deb?inline=false) build from source

**Reference :**   
  - [Building Vim from Source](https://github.com/Valloric/YouCompleteMe/wiki/Building-Vim-from-source)  
  - [checkinstall](https://manpages.debian.org/stretch/checkinstall/checkinstall.8.en.html)
  - [Enable +python support in vim](https://vi.stackexchange.com/questions/11526/how-to-enable-python-feature-in-vim/17502#17502) - `stackoverflow`

### Disclaimer
The package in this repo is provided without any warranty, express or implied. It is not guaranteed to be correct, bug-free or fit for any purpose. 
The author will not be held liable for any damage done by executing the package or for any illegal activities conducted with the help of the package.

Please use your own **discretion** and prefer **Method 1** download vim from Debian official repo.